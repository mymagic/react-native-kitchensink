# Kitchen Sink README - React Native version 0.21 #

### What is this repository for? ###

* To develop and explore React Native framework's capabilities and limits

### How do I get set up? ###

* Setup
  iOS Configuration
    1. OSX - for iOS development
    2. Homebrew is the recommended way to install Watchman and Flow
    3. Install Node.js 4.0 or newer
    4. brew install watchman (optional: might hit a node file watching bug)
    5. brew install flow (optional)

* Dependencies
    1. Android SDK
    2. Xcode 7.0
    3. iOS version 8.2 (for camera functionality)

* Required node_modules 
    1. Static Google Maps: npm install react-native-google-static-map --save 
    2. Camera: npm install react-native-camera --save
    3. Vector Icons: npm install react-native-vector-icons --save

* Miscellaneous
    1. Basic CrunchBase account from https://data.crunchbase.com/v3/page/pricing

### Guidelines ###

* Getting started: https://facebook.github.io/react-native/docs/getting-started.html#content
* Static Google Maps: http://staticmapmaker.com/google/
* Obtaining CrunchBase's user_key: https://data.crunchbase.com/docs/using-the-api