/** A Tinder-like swipe animation component **/
// author: Norman Low Wei Kit

'use strict';

import React, { 
	AppRegistry, 
	StyleSheet,
	Text,
	View,
	Animated,
	Component,
	PanResponder,
	Image
} from 'react-native';

import clamp from 'clamp';

const SWIPE_LIMIT = 120;	// the threshold before card goes off screen
var People = [ '#e0ffff', '#e6e6fa', 'gold', 'mistyrose', 'aquamarine' ]
var infinite = '\u221E';
var mugshot = require('./images/avatar.png');	// profile picture
var trollMessage;

var styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#C3D1D6',
  },
  card: {
    width: 310,
    height: 346,
    backgroundColor: 'red',
    borderRadius: 10,
    borderWidth: 3,
    borderColor: 'grey',
    padding: 22,
  },
  cardImage: {
  	height: 260,
    width: 260,
  },
  textLeft: {
  	marginTop: 20,
    position: 'absolute',
    left: 0,
    top: 0,
    fontSize: 18,
    color: 'darkslategrey',
    fontWeight: 'bold',
  },
  textRight: {
  	marginTop: 20,
    position: 'absolute',
    right: 0,
    top: 0,
    fontSize: 18,
    color: 'darkslategrey',
  },
  accept: {
  	position: 'absolute',
    borderColor: 'green',
    borderWidth: 2,
    borderRadius: 5,
    padding: 18,
    bottom: 20,
    right: 20,
    backgroundColor: 'white',
  },
  acceptText: {
    fontSize: 19,
    color: 'green',
  },
  reject: {
  	position: 'absolute',
    borderColor: 'red',
    borderWidth: 2,
    borderRadius: 5,
    bottom: 20,
    padding: 18,
    left: 20,
    backgroundColor: 'white',
  },
  rejectText: {
    fontSize: 19,
    color: 'red',
  },
  troll: {
    position: 'absolute',
    borderColor: 'blue',
    borderWidth: 2,
    borderRadius: 5,
    bottom: 20,
    padding: 18,
    right: 20,
    backgroundColor: 'white',
  },
  trollText: {
    fontSize: 19,
    color: 'blue',
  },
});


class CardSwipeTrollApp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pan: new Animated.ValueXY(),
      enter: new Animated.Value(0.5),
      person: People[0],
    } // end state
  } // end constructor

  goToNextPerson() {
    let currentPerson = People.indexOf(this.state.person);
    let nextPerson = currentPerson + 1;

    this.setState({
      person: People[ nextPerson > People.length - 1 ? 0 : nextPerson ]
    });
  } // end method

  componentDidMount() {
    this.animateEntrance();
  } // end method

  animateEntrance() {
    Animated.spring( this.state.enter, { toValue: 1, friction: 8 }).start();
  } // end method

  componentWillMount() {
    this._panResponder = PanResponder.create({
      onMoveShouldSetResponderCapture: () => true,	// become responder on touch
      onMoveShouldSetPanResponderCapture: () => true, // allow movements to be tracked

      onPanResponderGrant: (e, gestureState) => {
        this.state.pan.setOffset( { x: this.state.pan.x._value, y: this.state.pan.y._value } );
        this.state.pan.setValue( { x: 0, y: 0 } );
      },

      // User moves finger
      onPanResponderMove: Animated.event([
        null, { dx: this.state.pan.x, dy: this.state.pan.y },
      ]),

      // touchUp
      onPanResponderRelease: (e, { vx, vy }) => {
        this.state.pan.flattenOffset(); // imbue a spring animation and reset to 0 value
        var velocity;

        if ( vx >= 0 )
          velocity = clamp( vx, 3, 5 );
        else if ( vx < 0 )
          velocity = clamp( vx * -1, 3, 5 ) * -1;

        // Makes only the left swipe unable to decay
        if ( (this.state.pan.x._value) < SWIPE_LIMIT ) {
          Animated.spring(this.state.pan, {
            toValue: { x: 0, y: 0 },
            friction: 4, // bounciness
          }).start()
        } else if ( Math.abs(this.state.pan.x._value) > SWIPE_LIMIT) {
          Animated.decay(this.state.pan, {
            velocity: { x: velocity, y: vy },
            deceleration: 0.97, // rate of decay
          }).start(this.resetState.bind(this))
        } else {
          Animated.spring(this.state.pan, { // spring animation
            toValue: { x: 0, y: 0 },
            friction: 4, // bounciness
          }).start()
        } // end else
      } // end onPanResponderRelease
    })
  } // end componentWillMount

  resetState() {
    this.state.pan.setValue({ x: 0, y: 0 });
    this.state.enter.setValue(0);
    this.goToNextPerson();
    this.animateEntrance();
  } // end method

  render() {
    let { pan, enter, } = this.state;
    let [translateX, translateY] = [ pan.x, pan.y ];

    let rotate = pan.x.interpolate({ inputRange: [ -200, 0, 200 ], outputRange: [ "-30deg", "0deg", "30deg" ] });
    let opacity = pan.x.interpolate({ inputRange: [ -200, 0, 200 ], outputRange: [ 0.5, 1, 0.5 ] })
    let scale = enter;

    let animatedCardStyles = { transform: [{ translateX }, { translateY }, { rotate }, { scale } ], opacity };

    let rejectOpacity = pan.x.interpolate({ inputRange: [ -120, 0 ], outputRange: [ 1, 0 ] });
    let rejectScale = pan.x.interpolate({ inputRange: [ -150, 0 ], outputRange: [ 1, 0.5 ], extrapolate: 'clamp' });
    let animatedRejectStyles = { transform: [{ scale: rejectScale }], opacity: rejectOpacity }
    
    let trollOpacity = pan.x.interpolate({ inputRange: [ 120, 150 ], outputRange: [ 0, 1 ] });
    let trollScale = pan.x.interpolate({ inputRange: [ 0, 150 ], outputRange: [ 0.5, 1 ], extrapolate: 'clamp' });
    let animatedTrollStyles = { transform: [{ scale: trollScale }], opacity: trollOpacity }

    // Immediately-invoked function expressions definitions
    {(() => {
      switch (People.indexOf(this.state.person)) {
        case 0: trollMessage = <Text style={ styles.trollText }>Yes, swipe right!</Text>; break;
        case 1: trollMessage = <Text style={ styles.trollText }>It is me again.</Text>; break;
        case 2: trollMessage = <Text style={ styles.trollText }>Calling it a date.</Text>; break;
        case 3: trollMessage = <Text style={ styles.trollText }>You cannot pick anyone else.</Text>; break;
        case 4: trollMessage = <Text style={ styles.trollText }>I can feel your enthuasiasm.</Text>; break;
        default: trollMessage = null;
      } // end switch
    })()}

    return (
      <View style={ styles.container }>
        <Animated.View style={ [styles.card, animatedCardStyles, 
          { backgroundColor: this.state.person }] } { ...this._panResponder.panHandlers }>
        	<Image source={mugshot} style={ styles.cardImage } />
        	
        	<View>
        		<Text style={ styles.textLeft }>Hello, it is me.</Text>
        		<Text style={ styles.textRight }>{infinite} Connection</Text>
       		</View>
        </Animated.View>

        <Animated.View style={ [styles.reject, animatedRejectStyles] }>
          <Text style={ styles.rejectText }>You cannot reject me, yo.</Text>
        </Animated.View>

        <Animated.View style={ [styles.troll, animatedTrollStyles] }>
          {trollMessage}
        </Animated.View>
      </View>
    );
  } // end render
} // end class

module.exports = CardSwipeTrollApp;
