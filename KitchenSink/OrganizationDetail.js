/** A component that renders more details about the selected organization **/
// author: Norman Low Wei Kit

'use strict';

var React = require('react-native');
var {
	StyleSheet,
	Image,
	View,
	Text,
	Component,
	Dimensions
} = React;

var screenWidth = Dimensions.get('window').width;
var screenHeight = Dimensions.get('window').height;

var styles = StyleSheet.create({
	container: {
		alignSelf: 'stretch',
		marginTop: 72
	},
	bodyContainer: {
		margin: 20,
		alignSelf: 'stretch'
	},
	divider: {
		height: 1,
		backgroundColor: '#DDDDDD',
		marginTop: 8,
		marginBottom: 6
	},
	image: {
		flex: 1,
		width: screenWidth,
		height: 300,
		resizeMode: 'contain',
	},
	nameText: {
		fontSize: 20,
		fontWeight: 'bold',
		color: '#28b25d'
	},
	shortDesc: {
		fontSize: 18,
		color: '#656565'
	},
	cityDesc: {
		fontSize: 16,
		color: '#656565',
		fontWeight: 'bold'
	},
	desc: {
		fontSize: 16,
		color: '#656565',
		fontStyle: 'italic'
	},
});

class OrganizationDetail extends Component {
	render() {
		console.log('Details page rendered');
		var organization = this.props.organization.properties;	// store selected details
		console.log(organization);

		return (
			<View style={ styles.container }>
				<Image style={ styles.image } source={{ uri: organization.profile_image_url }} />
				<View style={ styles.bodyContainer }>
					<Text style={ styles.nameText }>{organization.name}</Text>
					<View style={ styles.divider }/>
					<Text style={ styles.cityDesc }>{organization.city_name + "\n"}</Text>
					<Text style={ styles.shortDesc }>{organization.short_description + "\n"}</Text>
					<Text style={ styles.desc }>{organization.homepage_url }</Text>
					<Text style={ styles.desc }>{organization.facebook_url }</Text>
					<Text style={ styles.desc }>{organization.twitter_url }</Text>
				</View>
			</View>
		);
	} // end render
} // end class

module.exports = OrganizationDetail;
