/** A Tinder-like swipe animation component **/
// author: Norman Low Wei Kit

'use strict';

import React, { 
  AppRegistry, 
  StyleSheet,
  Text,
  View,
  Animated,
  Component,
  PanResponder,
  Image
} from 'react-native';

import clamp from 'clamp';

const SWIPE_LIMIT = 120;  // the threshold before card goes off screen
var People = [ '#e0ffff', '#90ee90', 'gold', '#b0e0e6', 'khaki', '#87cefa', 'silver', 'red' ]
var mugshot;      // profile picture
var name;         // displayed name
var connections;  // counter

var styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#C3D1D6',
  },
  card: {
    width: 310,
    height: 346,
    backgroundColor: 'red',
    borderRadius: 10,
    borderWidth: 3,
    borderColor: 'grey',
    padding: 22,
  },
  cardImage: {
    height: 260,
    width: 260,
  },
  textLeft: {
    marginTop: 20,
    position: 'absolute',
    left: 0,
    top: 0,
    fontSize: 18,
    color: 'darkslategrey',
    fontWeight: 'bold',
  },
  textRight: {
    marginTop: 20,
    position: 'absolute',
    right: 0,
    top: 0,
    fontSize: 18,
    color: 'darkslategrey',
  },
  accept: {
    position: 'absolute',
    borderColor: 'green',
    borderWidth: 2,
    borderRadius: 5,
    padding: 18,
    bottom: 20,
    right: 20,
    backgroundColor: 'white',
  },
  acceptText: {
    fontSize: 19,
    color: 'green',
  },
  reject: {
    position: 'absolute',
    borderColor: 'red',
    borderWidth: 2,
    borderRadius: 5,
    bottom: 20,
    padding: 18,
    left: 20,
    backgroundColor: 'white',
  },
  rejectText: {
    fontSize: 19,
    color: 'red',
  },
});


class CardSwipeApp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pan: new Animated.ValueXY(),
      enter: new Animated.Value(0.5),
      person: People[0],
    } // end state
  } // end constructor

  goToNextPerson() {
    let currentPerson = People.indexOf(this.state.person);
    let nextPerson = currentPerson + 1;

    this.setState({
      person: People[ nextPerson > People.length - 1 ? 0 : nextPerson ]
    });
  } // end method

  componentDidMount() {
    this.animateEntrance();
  } // end method

  animateEntrance() {
    Animated.spring( this.state.enter, { toValue: 1, friction: 8 }).start();
  } // end method

  componentWillMount() {
    this._panResponder = PanResponder.create({
      onMoveShouldSetResponderCapture: () => true,  // become responder on touch
      onMoveShouldSetPanResponderCapture: () => true, // allow movements to be tracked

      onPanResponderGrant: (e, gestureState) => {
        this.state.pan.setOffset( { x: this.state.pan.x._value, y: this.state.pan.y._value } );
        this.state.pan.setValue( { x: 0, y: 0 } );
      },

      // User moves finger
      onPanResponderMove: Animated.event([
        null, { dx: this.state.pan.x, dy: this.state.pan.y },
      ]),

      // touchUp
      onPanResponderRelease: (e, { vx, vy }) => {
        this.state.pan.flattenOffset(); // imbue a spring animation and reset to 0 value
        var velocity;

        if ( vx >= 0 )
          velocity = clamp( vx, 3, 5 );
        else if ( vx < 0 )
          velocity = clamp( vx * -1, 3, 5 ) * -1;

        // Makes only the left swipe unable to decay
        if ( (this.state.pan.x._value) < SWIPE_LIMIT && this.state.person == 'silver') {
          Animated.spring(this.state.pan, {
            toValue: { x: 0, y: 0 },
            friction: 4, // bounciness
          }).start()
        } else if ( Math.abs(this.state.pan.x._value) > SWIPE_LIMIT) {
          Animated.decay(this.state.pan, {
            velocity: { x: velocity, y: vy },
            deceleration: 0.97, // rate of decay
          }).start(this.resetState.bind(this))
        } else {
          Animated.spring(this.state.pan, { // spring animation
            toValue: { x: 0, y: 0 },
            friction: 4, // bounciness
          }).start()
        } // end else
      } // end onPanResponderRelease
    })
  } // end componentWillMount

  resetState() {
    this.state.pan.setValue({ x: 0, y: 0 });
    this.state.enter.setValue(0);
    this.goToNextPerson();
    this.animateEntrance();
  } // end method

  render() {
    let { pan, enter, } = this.state;
    let [translateX, translateY] = [ pan.x, pan.y ];

    let rotate = pan.x.interpolate({ inputRange: [ -200, 0, 200 ], outputRange: [ "-30deg", "0deg", "30deg" ] });
    let opacity = pan.x.interpolate({ inputRange: [ -200, 0, 200 ], outputRange: [ 0.5, 1, 0.5 ] })
    let scale = enter;

    let animatedCardStyles = { transform: [{ translateX }, { translateY }, { rotate }, { scale } ], opacity };

    let acceptOpacity = pan.x.interpolate({ inputRange: [ 120, 150 ], outputRange: [ 0, 1 ] });
    let acceptScale = pan.x.interpolate({ inputRange: [ 0, 150 ], outputRange: [ 0.5, 1 ], extrapolate: 'clamp' });
    let animatedAcceptStyles = { transform: [{ scale: acceptScale }], opacity: acceptOpacity }

    let rejectOpacity = pan.x.interpolate({ inputRange: [ -120, 0 ], outputRange: [ 1, 0 ] });
    let rejectScale = pan.x.interpolate({ inputRange: [ -150, 0 ], outputRange: [ 1, 0.5 ], extrapolate: 'clamp' });
    let animatedRejectStyles = { transform: [{ scale: rejectScale }], opacity: rejectOpacity }

    // If else statements (optional)
    // https://facebook.github.io/react/tips/if-else-in-JSX.html

    // Immediately-invoked function expressions definitions
    {(() => {
      switch (People.indexOf(this.state.person)) {
        case 0:
          name = <Text style={ styles.textLeft }>Yee Siang</Text>; 
          mugshot = require('./images/yeesiang.jpg');
          connections = <Text style={ styles.textRight }>460 Connections</Text>;
          break;
        case 1:
          name = <Text style={ styles.textLeft }>MC</Text>;
          mugshot = require('./images/mc.jpg');
          connections = <Text style={ styles.textRight }>74 Connections</Text>;
          break;
        case 2: 
          name = <Text style={ styles.textLeft }>Fimi</Text>;
          mugshot = require('./images/fimi.png');
          connections = <Text style={ styles.textRight }>478 Connections</Text>;
          break;
        case 3:
          name = <Text style={ styles.textLeft }>Razlan</Text>;
          mugshot = require('./images/razlan.png');
          connections = <Text style={ styles.textRight }>500+ Connections</Text>;
          break;
        case 4: 
          name = <Text style={ styles.textLeft }>Adi</Text>;
          mugshot = require('./images/adi.jpg');
          connections = <Text style={ styles.textRight }>490 Connections</Text>;
          break;
        case 5:
          name = <Text style={ styles.textLeft }>Hafify</Text>;
          mugshot = require('./images/hafify-b.jpg');
          connections = <Text style={ styles.textRight }>131 Connections</Text>;
          break;
        case 6:
          name = <Text style={ styles.textLeft }>Norman</Text>;
          mugshot = require('./images/avatar.png');
          connections = <Text style={ styles.textRight }>&#8734; Connections</Text>;
          break;
        case 7:
          name = <Text style={ styles.textLeft }>Your crazy girl friend</Text>;
          mugshot = require('./images/crazygf.jpg');
          connections = null;
          break;
        default:
          name = <Text>Unknown</Text>;
          mugshot = null;
          connections = null;
      } // end switch
    })()} 

    return (
      <View style={ styles.container }>
        <Animated.View style={ [styles.card, animatedCardStyles, 
          { backgroundColor: 'white' }] } { ...this._panResponder.panHandlers }>
          <Image source={mugshot} style={ styles.cardImage } />

          <View>
            {name}
            {connections}
          </View>
        </Animated.View>

        <Animated.View style={ [styles.reject, animatedRejectStyles] }>
          <Text style={ styles.rejectText }>Nope!</Text>
        </Animated.View>

        <Animated.View style={ [styles.accept, animatedAcceptStyles] }>
          <Text style={ styles.acceptText }>Yes!</Text>
        </Animated.View>
      </View>
    );
  } // end render
} // end class

module.exports = CardSwipeApp;
