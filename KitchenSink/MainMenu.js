/** Main component that display all functionalities expected to perform **/
// author: Norman Low Wei Kit

'use strict';

var React = require('react-native');
var Camera = require('./Camera');
var GoogleStaticMap = require('./GoogleStaticMap');
var CardSwipe = require('./CardSwipe');
var CardSwipeTroll = require('./CardSwipeTroll');
var ButtonStyles = require('./ButtonStyles');
var StartupSearchMenu = require('./StartupSearchMenu');
var NativeWebView = require('./NativeWebView');

var { 
	StyleSheet,
	Text,
	TextInput,
	View,
	TouchableHighlight,
	ActivityIndicatorIOS,
	AppRegistry,
	Image,
	Component
} = React;

var styles = StyleSheet.create({
	description: { marginBottom: 20, fontSize: 18, textAlign: 'center', color: '#656565' },
	container: { padding: 30, marginTop: 65, alignItems: 'center' },
	flowRight: { flexDirection: 'row', alignItems: 'center', alignSelf: 'stretch' },
	buttonText: { fontSize: 18, color: 'white', alignSelf: 'center' },
	button: {
		height: 42,
		flex: 1,
		flexDirection: 'row',
		backgroundColor: '#48BBEC',
		borderColor: '#48BBEC',
		borderWidth: 1,
		borderRadius: 8,
		marginBottom: 12,
		alignSelf: 'stretch',
		justifyContent: 'center'
	},
	searchInput: {
		height: 36,
		padding: 4,
		marginRight: 5,
		flex: 4,
		fontSize: 18,
		borderWidth: 1,
		borderRadius: 8,
		borderColor: '#48BBEC',
		color: '#48BBEC'
	},
});


class MainMenu extends Component {

	onCameraPressed() {
		console.log('Toggle camera function');
		this.props.navigator.push({
			title: "Camera",
			component: Camera
		});
	} // end method

	onMapPressed() {
		console.log('Toggle static map function');
		this.props.navigator.push({
			title: "Google Static Maps",
			component: GoogleStaticMap
		});
	} // end method

	onCardPressed() {
		console.log('Card swipe function');
		this.props.navigator.push({
			title: "Card Swipe",
			component: CardSwipe
		});
	} // end method

	onCardTrollPressed() {
		console.log('Card swipe troll function');
		this.props.navigator.push({
			title: "Card Swipe T",
			component: CardSwipeTroll
		});
	} // end method

	onButtonStylesPressed() {
		console.log('Button styling view function');
		this.props.navigator.push({
			title: "Button Styles",
			component: ButtonStyles
		});
	} // end method

	onStartupPressed() {
		console.log('View startup list function');
		this.props.navigator.push({
			title: "Startup Search",
			component: StartupSearchMenu
		});
	} // end method

	onWebViewPressed() {
		console.log('Native WebView function');
		this.props.navigator.push({
			title: "Native WebView",
			component: NativeWebView
		});
	} // end method

		render() {
			console.log('MainMenu rendered');
			return (
				<View style={styles.container}>
					<Text style={styles.description}>
						RN demo app showcasing different functionalities
			        </Text>

			        <TouchableHighlight style={styles.button} onPress={this.onMapPressed.bind(this)}>
			        	<Text style={styles.buttonText}>
			        		Google Static Maps
			        	</Text>
			        </TouchableHighlight>

			        <TouchableHighlight style={styles.button} onPress={this.onCameraPressed.bind(this)}>
			        	<Text style={styles.buttonText}>
			        		Camera
			        	</Text>
			        </TouchableHighlight>

			        <TouchableHighlight style={styles.button} onPress={this.onCardPressed.bind(this)}>
			        	<Text style={styles.buttonText}>
			        		Tinder-like Swipe
			        	</Text>
			        </TouchableHighlight>

			        <TouchableHighlight style={styles.button} onPress={this.onCardTrollPressed.bind(this)}>
			        	<Text style={styles.buttonText}>
			        		Card Swipe T
			        	</Text>
			        </TouchableHighlight>

			        <TouchableHighlight style={styles.button} onPress={this.onButtonStylesPressed.bind(this)}>
			        	<Text style={styles.buttonText}>
			        		Button Styles
			        	</Text>
			        </TouchableHighlight>

			        <TouchableHighlight style={styles.button} onPress={this.onStartupPressed.bind(this)}>
			        	<Text style={styles.buttonText}>
			        		Startup Search
			        	</Text>
			        </TouchableHighlight>

			        <TouchableHighlight style={styles.button} onPress={this.onWebViewPressed.bind(this)}>
			        	<Text style={styles.buttonText}>
			        		WebView
			        	</Text>
			        </TouchableHighlight>

			        <TouchableHighlight style={styles.button}>
			        	<Text style={styles.buttonText}>
			        		
			        	</Text>
			        </TouchableHighlight>

			        <TouchableHighlight style={styles.button}>
			        	<Text style={styles.buttonText}>
			        		
			        	</Text>
			        </TouchableHighlight>

			        <TouchableHighlight style={styles.button}>
			        	<Text style={styles.buttonText}>
			        		
			        	</Text>
			        </TouchableHighlight>

				</View>
		    );
		} // end render
} // end class

module.exports = MainMenu;

