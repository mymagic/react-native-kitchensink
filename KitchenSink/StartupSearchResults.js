/** A component that renders a ListView of startups **/
// author: Norman Low Wei Kit

'use strict';

var React = require('react-native');
var OrganizationDetail = require('./OrganizationDetail');

var {
	StyleSheet,
	ListView,
	Image,
	View,
	Text,
	TouchableHighlight,
	Component
} = React;

var styles = StyleSheet.create({
	thumbnail: {
		width: 86,
		height: 86,
		marginRight: 10,
		resizeMode: 'contain'
	},
	textContainer: {
		flex: 1,
		marginLeft: 5
	},
	divider: {
		height: 1,
		backgroundColor: '#dddddd'
	},
	nameText: {
		fontSize: 20,
		fontWeight: 'bold',
		color: '#28b25d'
	},
	cityText: {
		fontSize: 18,
		color: '#656565'
	},
	domainText: {
		fontSize: 18,
		color: '#656565'
	},
	rowContainer: {
		flexDirection: 'row',
		padding: 10
	}
});

class StartupSearchResults extends Component {
	constructor(props) {
		super(props);

		// supply data to the ListView via .DataSource
		var dataSource = new ListView.DataSource({
			rowHasChanged: (r1, r2) => r1.uuid !== r2.uuid}); // only re-render changed rows
		this.state = { dataSource: dataSource.cloneWithRows(this.props.items) };
	} // end constructor

	renderRow(rowData, sectionID, rowID) {

		return (
			// arrow function used to capture the guid for the row
			<TouchableHighlight onPress={() => this.rowPressed(rowData.uuid)} underlayColor='#1edf7e'>
				<View>
					<View style={styles.rowContainer}>
						<Image style={styles.thumbnail} source={{ uri: rowData.properties.profile_image_url }} />
						<View style={ styles.textContainer}>
							<Text style={styles.nameText} numberOfLines={2}>{rowData.properties.name}</Text>
							<Text style={styles.cityText} numberOfLines={1}>{rowData.properties.city_name}</Text>
							<Text style={styles.domainText} numberOfLines={1}>{rowData.properties.domain}</Text>
						</View>
					</View>
					<View style={styles.divider}/>
				</View>
			</TouchableHighlight>
		);
	} // end renderRow

	render() {
		return (
			<ListView dataSource={ this.state.dataSource } renderRow={ this.renderRow.bind(this) }/>
		);
	} // end render

	rowPressed(organizationUuid) {
		console.log('Selected uuid - ' + organizationUuid);
		var organization = this.props.items.filter(prop => prop.uuid === organizationUuid)[0];

		this.props.navigator.push({
			title: "Organization Detail",
			component: OrganizationDetail,
			passProps: { organization: organization }
		});
	} // end method

} // end class

module.exports = StartupSearchResults;
