/** Map view component to view static location **/
// author: Norman Low Wei Kit

import React from 'react-native';

const {
    AppRegistry,
    StyleSheet,
    View,
    Text,
    Component
    } = require('react-native');

import GoogleStaticMap from 'react-native-google-static-map';

const mockLocation = {
  latitude: '2.909744',
  longitude: '101.654934',
  zoom: 16,
  size: { width: 400, height: 550 }
} // end const

// Properties
// latitude         | string | latitude point
// longitude        | string | longitude point
// size             | object | the image size
// format           | string | 'png', 'png32', 'jpg', 'gif'
// zoom             | number | defines map zoom level
// mapType | string | string | 'roadmap', 'satelllite', 'terrain', 'hybrid'
// hasCenterMarker  | bool   | add a marker on the center.default is true


class MapApp extends Component {
  static propTypes = {};

  render() {
    return (
        <View style={styles.content}>

          <Text style={styles.title}>
            Google Static Maps
          </Text>

          <GoogleStaticMap style={styles.map} {...mockLocation} />

        </View>
    );
  } // end render
} // end class

const styles = StyleSheet.create({
  content: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#4d4d4d'
  },

  title: {
    color: 'white',
    fontSize: 30,
    paddingVertical: 12
  },

  map: {
    borderRadius: 12,
    borderColor: 'black',
    borderWidth: 0
  }

});

module.exports = MapApp;

