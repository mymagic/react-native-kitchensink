/** An ES6 component to demonstrate native WebView **/

'use strict';

var React = require('react-native');

var {
	StyleSheet,
	Text,
	TextInput,
	TouchableOpacity,
	View,
	WebView,
	Component
} = React;

var textInputReference = 'urlInput';
var webviewReference = 'webview';
var coloredOn = 'rgba(255, 255, 255, 1)';
var coloredOff = 'rgba(255, 255, 255, 0.5)';

var styles = StyleSheet.create({
	container: {
		paddingTop: 70,
		flex: 1,
		backgroundColor: '#96877b'
	},
	addressBarRow: {
		flexDirection: 'row',
		padding: 8
	},
	addressBarTextInput: {
		flex: 1,
		height: 24,
		fontSize: 14,
		borderRadius: 3,
		borderWidth: 1,
		paddingLeft: 12,
		paddingTop: 3,
		paddingBottom: 3,
		borderColor: 'transparent',
		backgroundColor: coloredOn
	},
	btnEnabled: {
		width: 40,
		height: 24,
		padding: 3,
		marginRight: 3,
		borderRadius: 3,
		alignItems: 'center',
		justifyContent: 'center',
		borderColor: 'transparent',
		backgroundColor: coloredOn
	},
	btnDisabled: {
		width: 40,
		height: 24,
		padding: 3,
		marginRight: 3,
		borderRadius: 3,
		alignItems: 'center',
		justifyContent: 'center',
		borderColor: 'transparent',
		backgroundColor: coloredOff
	},
	btnGo: {
		width: 26,
		height: 24,
		padding: 3,
		marginLeft: 8,
		borderRadius: 3,
		alignItems: 'center',
		borderColor: 'transparent',
		alignSelf: 'stretch',
		backgroundColor: coloredOn
	},
	statusBar: {
		height: 18,
		paddingLeft: 8,
		flexDirection: 'row',
		alignItems: 'center',
	},
	statusBarText: {
		color: 'white',
		fontSize: 13
	},
	spinner: {
		width: 22,
		marginRight: 6
	},
});


class NativeWebView extends Component {
	constructor(props) {
		super(props);
		this.state = {
			url: 'https://facebook.github.io/react-native/',
			status: 'No Page Loaded',
			backButtonEnabled: false,
			forwardButtonEnabled: false,
			loading: true,
			scalesPageToFit: true,
			inputText: ''
		};
	} // end constructor

	onNavigationStateChange(navState) {
		this.setState({
			url: navState.url,
			status: navState.title,
			backButtonEnabled: navState.canGoBack,
			forwardButtonEnabled: navState.canGoForward,
			loading: navState.loading,
			scalesPageToFit: true
		});
	} // end method

	textInputHandler(event) {
		var url = event.nativeEvent.text;	// store typed input
		var regex = /[a-zA-Z-_]+:/;
		if (!regex.test(url))
			url = 'http://' + url;
		this.inputText = url;
		console.log('Text handler - ' + this.inputText);
	} // end method

	goButtonPressed() {
		var inputURL = this.inputText.toLowerCase();
		if (inputURL === this.state.url)
			this.reload();	// refresh
		else this.setState({ url: inputURL });
		
		console.log('Opening link - ' + this.state.url);
		this.refs[textInputReference].blur();	// hide keyboard
	} // end method

	goBack() {
		this.refs[webviewReference].goBack();
	} // end method

	goForward() {
		this.refs[webviewReference].goForward();
	} // end method

	reload() {
		this.refs[webviewReference].reload();
	} // end method

	onShouldStartLoadWithRequest(event) {
		return true;
	} // end method

	render() {
		this.inputText = this.state.url;

		return (
			<View style={styles.container}>
				<View style={styles.statusBar}>
					<Text style={styles.statusBarText}>{this.state.status}</Text>
				</View>

				<View style={styles.addressBarRow}>
					<TouchableOpacity 
						onPress={this.goBack.bind(this)}
						style={this.state.backButtonEnabled ? styles.btnEnabled : styles.btnDisabled}>
						<Text>{'<'}</Text>
					</TouchableOpacity>

					<TouchableOpacity 
						onPress={this.goForward.bind(this)}
						style={this.state.forwardButtonEnabled ? styles.btnEnabled : styles.btnDisabled}>
						<Text>{'>'}</Text>
					</TouchableOpacity>

					<TextInput
						ref={textInputReference}
						style={styles.addressBarTextInput}
						defaultValue={this.state.url}
						autoCapitalize="none"
						clearButtonMode="while-editing"
						onSubmitEditing={this.goButtonPressed.bind(this)}
						onChange={this.textInputHandler.bind(this)} />

					<TouchableOpacity onPress={this.goButtonPressed.bind(this)}>
						<View style={styles.btnGo}>
							<Text>Go</Text>
						</View>
					</TouchableOpacity>
				</View>
				
				<WebView
					ref={webviewReference}
					source={{uri: this.state.url}}
					style={styles.webView}
					automaticallyAdjustContentInsets={false}
					startInLoadingState={true}
					javaScriptEnabled={true}
					domStorageEnabled={true}
					decelerationRate="normal"
					onNavigationStateChange={this.onNavigationStateChange.bind(this)}
					onShouldStartLoadWithRequest={this.onShouldStartLoadWithRequest.bind(this)}
					scalesPageToFit={this.state.scalesPageToFit} />

			</View>
		);
	} // end render
} // end class

module.exports = NativeWebView;

