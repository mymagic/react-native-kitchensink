/** A component intended to display various styling of a button and animation **/

'use strict';

var React = require('react-native');
var Icon = require('react-native-vector-icons/FontAwesome');

var {
	Platform,
	TouchableHighlight,
	TouchableNativeFeedback,
	StyleSheet,
	Text,
	TextInput,
	View,
	AppRegistry,
	Image,
	Component,
	ScrollView,
} = React;

var styles = StyleSheet.create({
	container: { 
		padding: 30, 
		marginTop: 65, 
		alignItems: 'center' 
	},

	btnText: { 
		fontFamily: 'Arial', 
		fontSize: 18, 
		color: 'white', 
		alignSelf: 'center' 
	},
	
	btnCustomText: { 
		fontFamily: 'Arial', 
		fontSize: 20, 
		color: 'white', 
		fontWeight: 'bold', 
		padding: 9 
	},

	btnViewContainer: { 
		marginBottom: 12, 
		alignSelf: 'center' 
	},

	btnRegular: {
		height: 46,
		flex: 1,
		flexDirection: 'row',
		backgroundColor: '#36c018',
		marginBottom: 12,
		alignSelf: 'stretch',
		justifyContent: 'center'
	},

	btnBordered: {
		height: 46,
		flex: 1,
		flexDirection: 'row',
		backgroundColor: '#393854',
		borderColor: '#a9b111',
		borderWidth: 4,
		marginBottom: 12,
		alignSelf: 'stretch',
		justifyContent: 'center'
	},

	btnRounded: {
		height: 42,
		flex: 1,
		flexDirection: 'row',
		backgroundColor: '#da1e20',
		borderColor: '#da1e20',
		borderWidth: 1,
		borderRadius: 8,
		marginBottom: 12,
		alignSelf: 'stretch',
		justifyContent: 'center'
	},

	btnRoundedBordered: {
		height: 46,
		flex: 1,
		flexDirection: 'row',
		backgroundColor: '#f36136',
		marginBottom: 12,
		alignSelf: 'stretch',
		justifyContent: 'center',
		borderColor: '#a8a8a8',
		borderWidth: 4,
		borderRadius: 40,
	},

	btnRoundedBorderedMorphed: {
		height: 46,
		flex: 1,
		flexDirection: 'row',
		backgroundColor: '#8c2298',
		marginBottom: 12,
		alignSelf: 'stretch',
		justifyContent: 'center',
		borderColor: '#a8a8a8',
		borderWidth: 4,
		borderRadius: 40,
	},

	btnFacebookIcon: {
		backgroundColor: '#2c4388',
		height: 50,
		justifyContent: 'center',
		alignSelf: 'stretch',
	},

	btnTwitterIcon: {
		backgroundColor: '#4398ed',
	},

	btnCustomIcon: {
  		width: 40,
  		height: 40,
  	},

	btnCustomContainer1: {
	    width: 300,
	    height: 60,
	    backgroundColor: '#1f6387',
	    borderRadius: 10,
	    borderColor: 'grey',
	    padding: 10,
	    flexDirection: 'row',
	    justifyContent: 'flex-start',
	    alignSelf: 'center',
  	},

	btnCustomContainer2: {
	    width: 300,
	    height: 60,
	    backgroundColor: '#1f6387',
	    borderRadius: 10,
	    borderColor: 'grey',
	    padding: 10,
	    flexDirection: 'row',
	    justifyContent: 'center',
	    alignSelf: 'center',
  	},

  	btnCustomContainer3: {
	    width: 300,
	    height: 60,
	    backgroundColor: '#1f6387',
	    borderRadius: 10,
	    borderColor: 'grey',
	    padding: 10,
	    flexDirection: 'row',
	    justifyContent: 'flex-end',
	    alignSelf: 'center',
  	},

  	scrollView: {
  		flex: 1,
  		alignSelf: 'stretch'
  	},
});

class ButtonStyles extends Component {
	constructor(props) {
		super(props);
		this.state = { onClicked: false };
		this.handlerButtonOnClick = this.handlerButtonOnClick.bind(this);
	} // end constructor

	handlerButtonOnClick() {
		this.setState({ onClicked: true });
	} // end method

	render() {
		var TouchableElement = TouchableHighlight;
		if (Platform.OS === 'android')
			TouchableElement = TouchableNativeFeedback;

		if (this.state.onClicked)
			styles.btnRoundedBordered = styles.btnRoundedBorderedMorphed;

		return (
			<View style={styles.container}>
				<ScrollView style={ styles.scrollView } automaticallyAdjustContentInsets={false}>
					<TouchableElement style={ styles.btnRegular }>
						<Text style={ styles.btnText }>Regular</Text>
					</TouchableElement>

					<TouchableElement style={ styles.btnBordered }>
						<Text style={ styles.btnText }>Bordered</Text>
					</TouchableElement>

					<TouchableElement style={ styles.btnRounded }>
						<Text style={ styles.btnText }>Rounded Corner</Text>
					</TouchableElement>

					<TouchableElement style={ styles.btnRoundedBordered } onPress={ this.handlerButtonOnClick }>
						<Text style={ styles.btnText }>Rounded Corner Bordered</Text>
					</TouchableElement>
						
					<View style={ styles.btnViewContainer }>
						<Icon.Button name="facebook" style={ styles.btnFacebookIcon }>
							<Text style={ styles.btnText }>Login with Facebook</Text>
						</Icon.Button>
					</View>

					<View style={ styles.btnViewContainer }>
						<Icon.Button size={40} name="twitter" style={ styles.btnTwitterIcon }>
							<Text style={ styles.btnText }>Login with Twitter</Text>
						</Icon.Button>
					</View>

					<View style={ styles.btnViewContainer }>
						<TouchableElement>
							<View style={ styles.btnCustomContainer1 }>
								<Image source={ require('./images/arrow-left.png') } style={ styles.btnCustomIcon }/>
								<Text style={ styles.btnCustomText }>Left aligned</Text>
							</View>
						</TouchableElement>
					</View>

					<View style={ styles.btnViewContainer }>
						<TouchableElement>
							<View style={ styles.btnCustomContainer2 }>
								<Text style={ styles.btnCustomText }>Center</Text>
								<Image source={ require('./images/arrow-up.png') } style={ styles.btnCustomIcon }/>
								<Text style={ styles.btnCustomText }>aligned</Text>
							</View>
						</TouchableElement>
					</View>

					<View style={ styles.btnViewContainer }>
						<TouchableElement>
							<View style={ styles.btnCustomContainer3 }>
								<Text style={ styles.btnCustomText }>Right aligned</Text>
								<Image source={ require('./images/arrow-right.png') } style={ styles.btnCustomIcon }/>
							</View>
						</TouchableElement>
					</View>

				</ScrollView>
			</View>
		);
	} // end render
} // end class

module.exports = ButtonStyles;
