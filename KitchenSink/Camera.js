/** Camera component to capture image **/
/** Requires iOS version 8.2 and physical device to work **/
// author: Norman Low Wei Kit

'use strict';

import React, {
  AppRegistry,
  Component,
  Dimensions,
  StyleSheet,
  Text,
  TouchableHighlight,
  View
} from 'react-native';

import Camera from 'react-native-camera';

class CameraApp extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Camera ref={(cam) => { this.camera = cam; }}
          style={styles.preview}
          aspect={Camera.constants.Aspect.Fill}>

          <Text style={styles.capture} onPress={this.takePicture.bind(this)}>
            Capture
          </Text>

        </Camera>
      </View>
    );
  } // end render

  takePicture() {
    this.camera.capture()
      .then((data) => console.log(data))
      .catch(err => console.error(err));
  } // end method
} // end class

const styles = StyleSheet.create({
  container: { flex: 1 },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    color: '#000',
    padding: 10,
    margin: 40,
    fontSize: 20,
    borderRadius: 5,
  }
});

module.exports = CameraApp;

