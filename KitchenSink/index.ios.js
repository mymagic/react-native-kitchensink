/** This is a demo application showcasing different React Native functionalities **/
// author: Norman Low Wei Kit


'use strict'; // enable Strict Mode: adds improved error handling 

var React = require('react-native');  // loads RN module for function calling later
var MainMenu = require('./MainMenu');

var { 
  AppRegistry, 
  StyleSheet, 
  Text, 
  View,
} = React; // assign multiple object properties to a single variable

// Apply styles to the view's content
var styles = StyleSheet.create({
  container: { flex: 1 },
  welcome: { fontSize: 20, textAlign: 'center', margin: 10 },
  text: { color: 'black', backgroundColor: 'white', fontSize: 30, margin: 30 },   
});

// class KitchenSink extends React.Component { // alternate way to define
//   render() {                                // alternate way to define
var KitchenSink = React.createClass({
  render: function() {
    return (
      
      <React.NavigatorIOS         // construct an iOS navigation bar
        style={styles.container}  // applies a style
        initialRoute={{           // set the path to the component
          title: 'Kitchen Sink',
          component: MainMenu
        }}/>

    ); // end return
  } // end render
}); // end var

// Application entry point
// AppRegistry.registerComponent('KitchenSink', () => KitchenSink); // alternate declaration
React.AppRegistry.registerComponent('KitchenSink', function() { return KitchenSink });

