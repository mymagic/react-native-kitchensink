/** A component that runs queries to get a list of startups through API call **/
// author: Norman Low Wei Kit

'use strict';

var React = require('react-native');

var StartupSearchResults = require('./StartupSearchResults');

var { 
	StyleSheet,
	Text,
	TextInput,
	View,
	TouchableHighlight,
	ActivityIndicatorIOS,
	Image,
	Component,
	WebView,
	Linking
} = React;

var styles = StyleSheet.create({
	header: {
		marginBottom: 3,
		fontSize: 20,
		textAlign: 'center',
		color: '#656565',
	},
	description: {
		marginBottom: 20,
		fontSize: 18,
		textAlign: 'center',
		color: '#656565',
		fontStyle: 'italic'
	},
	attribution: {
		marginBottom: 26,
		fontSize: 14,
		textAlign: 'center',
		color: '#656565',
		fontStyle: 'italic'
	},
	container: {
		padding: 30,
		marginTop: 65,
		alignItems: 'center'
	},
	descContainer: {
		marginBottom: 13,
	},
	flowRight: {
		flexDirection: 'row',
		alignItems: 'center',
		alignSelf: 'stretch'
	},
	btnText: {
		fontSize: 19,
		color: 'white',
		alignSelf: 'center',
		fontWeight: 'bold',
	},
	btnSearch: {
		height: 46,
		flex: 1,
		flexDirection: 'row',
		backgroundColor: '#28b25d',
		borderColor: '#28b25d',
		borderWidth: 1,
		borderRadius: 8,
		marginBottom: 10,
		alignSelf: 'stretch',
		justifyContent: 'center'
	},
	searchInput: {
		height: 46,
		padding: 4,
		marginRight: 5,
		marginBottom: 10,
		flex: 4,
		fontSize: 18,
		borderWidth: 1,
		borderColor: '#28b25d',
		borderRadius: 8,
		color: '#28b25d',
		textAlign: 'center'
	},
	webView: { 
		backgroundColor: '#28b25d', 
		height: 350
	},
});


function urlForQueryAndPage(key, value, inputOrganizationType, inputKeyword, pageNumber ) {
// key is which property is being search for; value is user input

	var userKey = ''; // API user key

	if (inputKeyword !== null) {	// then search based on keyword
		var data = {
			// updated_since: '',
			name: inputKeyword,
			// domain_name: '',
			// locations: value,
			organization_types: inputOrganizationType,
			// sort_order: '',
			page: pageNumber,
			user_key: userKey
		};
	} // end if

	else {
		var data = {
			// updated_since: '',
			// name: '',
			// domain_name: '',
			locations: value,
			organization_types: inputOrganizationType,
			// sort_order: '',
			page: pageNumber,
			user_key: userKey
		};
		data[key] = value;
	} // end else

	var queryString = Object.keys(data)
		.map(key => key + '=' + encodeURIComponent(data[ key ]))
		.join('&');

	return 'https://api.crunchbase.com/v/3/odm-organizations?' + queryString;
};

class StartupSearchMenu extends Component {
	constructor(props) {
		super(props);
		this.state = {
			inputKeyword: null,
			inputCountry: 'Malaysia', // default
			inputOrganizationType: 'company',
			isLoading: false,	// keep track of query status
			message: ''
		}
	} // end constructor

	// Event handler
	onSearchCountryChanged(event) {
		// console.log('onSearchCompanyChanged triggered');
		this.setState({ inputCountry: event.nativeEvent.text }); // update state
		// console.log('inputCountry: ' + this.state.inputCountry);
	} // end method

	onSearchCompanyChanged(event) {
		this.setState({ inputOrganizationType: event.nativeEvent.text });
	} // end method

	onSearchKeywordChanged(event) {
		this.setState({ inputKeyword: event.nativeEvent.text });
	} // end method

	onSearchPressed() {
		this.setState({ message: '' });
		var query = urlForQueryAndPage( 'locations', this.state.inputCountry, 
			this.state.inputOrganizationType, this.state.inputKeyword, 1 );
		this.executeQuery(query);
	} // end method

	executeQuery(query) {
		console.log('executeQuery func - ' + query);
		this.setState({ isLoading: true });
		fetch(query)
			.then(data => data.json())
			.then(json => this.handleResponse(json.data))
			.catch(error => this.setState({
					isLoading: false,
					message: '' + error
			}));
	} // end method

	handleResponse(data) {
		this.setState({ isLoading: false, message: '' });
		if (data.paging.total_items) {
			console.log('Organization(s) found: ' + data.paging.total_items);
			
			this.props.navigator.push({
				title: 'Results',
				component: StartupSearchResults,
				passProps: { items: data.items }
			});
		} else
		this.setState({ message: 'Data unavailable.' });
	} // end method

	openHyperlink() {
		var url = 'http://www.crunchbase.com';
		Linking.openURL(url).catch(err => console.error('Error accessing the internet', err));
	}

	render() {
		var spinner = this.state.isLoading ? 
			( <ActivityIndicatorIOS hidden='true' size='large' /> ) : ( <View/> );

		return (
			<View style={styles.container}>
				<Text style={ styles.header }>Search for organization(s)</Text>
				<Text style={ styles.attribution } onPress={this.openHyperlink.bind(this)}>CrunchBase</Text>
				
				<View style={styles.flowRight}>
					<TextInput 
						style={ styles.searchInput }
						onChange={ this.onSearchCountryChanged.bind(this) }
						placeholder='Enter country name' />
				</View>

				<View style={ styles.descContainer }>
					<Text style={ styles.description }>"Malaysia/Japan/Atlantis"</Text>
				</View>

				<View style={ styles.flowRight }>
					<TextInput 
						style={ styles.searchInput }
						onChange={ this.onSearchCompanyChanged.bind(this) }
						placeholder='Enter organization type' />
				</View>

				<View style={ styles.descContainer }>
					<Text style={ styles.description }>"Company/Investor/School"</Text>
				</View>

				<View style={ styles.flowRight }>
					<TextInput 
						style={ styles.searchInput }
						onChange={ this.onSearchKeywordChanged.bind(this) }
						placeholder='Enter keyword' />
				</View>

				<View style={ styles.descContainer }>
					<Text style={ styles.description }>e.g "Activision Blizzard/Facebook"</Text>
				</View>

				<TouchableHighlight 
					style={ styles.btnSearch } 
					underlayColor='#99d9f4' 
					onPress={ this.onSearchPressed.bind(this) }>
					<Text style={ styles.btnText }>Search Now</Text>
				</TouchableHighlight>

				{spinner}
				<Text style={ styles.description }>{ this.state.message }</Text>
			</View>

		);
	} // end render
} // end class

module.exports = StartupSearchMenu;
